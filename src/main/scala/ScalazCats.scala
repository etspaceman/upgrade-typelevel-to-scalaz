// Copyright: 2018 - 2018 Sam Halliday
// License: https://opensource.org/licenses/BSD-3-Clause

package prelude

import scalaz._, Scalaz._
import scalaz.Liskov.<~<
import com.github.ghik.silencer.silent

/**
 * Provides extension methods to scalaz core data types and syntax to
 * typeclasses that matches the cats renamings.
 *
 * Useful when upgrading a codebase from cats to scalaz.
 */
@SuppressWarnings(
  Array("org.wartremover.warts.OptionPartial", "org.wartremover.warts.Throw")
)
trait ScalazCats {
  type Validated[A, B]    = Validation[A, B]
  type ValidatedNel[E, X] = Validation[NonEmptyList[E], X]
  type Valid[A]           = Success[A]
  type Invalid[A]         = Failure[A]

  @inline final val Valid: Success.type   = Success
  @inline final val Invalid: Failure.type = Failure
  final object Validated {
    // yes, repeated...
    type Valid[A]   = Success[A]
    type Invalid[A] = Failure[A]

    def valid[A](a: A): Success[A]   = Success(a)
    def invalid[E](e: E): Failure[E] = Failure(e)

    def fromOption[E, A](o: Option[A], ifNone: =>E): Validated[E, A] =
      o.fold(ifNone.failure[A])(_.success[E])

    def fromEither[E, A](e: Either[E, A]): Validated[E, A] =
      e.fold(_.failure, _.success)
  }
  def valid[E, A](a: A): Validation[E, A]         = Success(a)
  def validNel[E, A](a: A): ValidationNel[E, A]   = a.successNel
  def invalid[E, A](e: E): Validation[E, A]       = Failure(e)
  def invalidNel[E, A](e: E): ValidationNel[E, A] = e.failureNel

  type Ior[A, B] = \&/[A, B]
  object Ior {
    @inline final val Left: \&/.This.type  = \&/.This
    @inline final val Right: \&/.That.type = \&/.That
    @inline final val Both: \&/.Both.type  = \&/.Both
  }

  implicit class ListCatsOps[A](val list: List[A]) {
    def unsafeToNel: NonEmptyList[A] = list.toNel.get
  }

  implicit class IListCatsOps[A](val list: IList[A]) {
    def unsafeToNel: NonEmptyList[A] = list.toNel.get
  }

  implicit class NonEmptyListCatsOps[A](val nel: NonEmptyList[A]) {
    def toNonEmptyStream: OneAnd[EphemeralStream, A] =
      OneAnd(nel.head, nel.tail.toEphemeralStream)

    def nonEmptyPartition[B, C](
      f: A => Either[B, C]
    ): NonEmptyList[B] \&/ NonEmptyList[C] = {
      val reversed = nel.reverse
      val lastThese =
        f(reversed.head)
          .bimap(NonEmptyList(_), NonEmptyList(_))
          .disjunction
          .toThese

      reversed.tail.foldLeft(lastThese)(
        (these, a) =>
          (f(a), these) match {
            case (Right(c), \&/.This(_)) => these.putRight(NonEmptyList(c))
            case (Right(c), _)           => these.rightMap(c <:: _)
            case (Left(b), \&/.That(r))  => (NonEmptyList(b), r).both
            case (Left(b), _)            => these.leftMap(b <:: _)
          }
      )
    }

    def reduceLeft[AA >: A](f: (AA, AA) => AA): AA =
      nel.tail.foldLeft[AA](nel.head)(f)

    def concatNel(other: NonEmptyList[A]): NonEmptyList[A] = nel |+| other
  }

  implicit class BindCatsOps[F[_], A](val self: F[A])(implicit M: Bind[F]) {
    def flatTap[B](f: A => F[B]): F[A] = self >>! f
  }

  implicit class MonadErrorCatsOps[F[_], E, A](
    val self: F[A]
  )(implicit M: MonadError[F, E]) {

    def partiallyRecover(pf: PartialFunction[E, A]): F[A] =
      M.handleError(self)(
        s => pf.andThen(x => M.pure(x)).applyOrElse(s, M.raiseError)
      )

    def partiallyRecoverWith(pf: PartialFunction[E, F[A]]): F[A] =
      M.handleError(self)(
        s => pf.applyOrElse(s, M.raiseError)
      )

    def partiallyErrorMap(pf: PartialFunction[E, E]): F[A] =
      M.bind(self.attempt)(
        _.fold(
          e => M.raiseError(pf.applyOrElse[E, E](e, _ => e)),
          x => M.pure(x)
        )
      )

    def onError(f: E => F[Unit]): F[A] =
      M.handleError(self)(
        e => M.bind(M.handleError(f(e))(_ => M.point(())))(_ => M.raiseError(e))
      )

    def partiallyOnError(pf: PartialFunction[E, F[Unit]]): F[A] =
      M.handleError(self)(
        e =>
          pf.andThen(M.apply2(_, M.raiseError[A](e))((_, b) => b))
            .applyOrElse(e, M.raiseError)
      )

    def ensure(error: =>E)(predicate: A => Boolean): F[A] =
      M.bind(self)(a => if (predicate(a)) M.pure(a) else M.raiseError(error))

    def ensureOr(error: A => E)(predicate: A => Boolean): F[A] =
      M.bind(self)(a => if (predicate(a)) M.pure(a) else M.raiseError(error(a)))
  }

  // bless me father...
  type ZIO[E, A] = scalaz.ioeffect.IO[E, A]
  @inline final val ZIO: scalaz.ioeffect.IO.type = scalaz.ioeffect.IO
  type IO[A] = scalaz.ioeffect.Task[A]
  object IO {
    type Par[a] = scalaz.ioeffect.Task[a] @@ scalaz.Tags.Parallel

    final def apply[A](effect: =>A): IO[A] = ZIO.syncThrowable(effect)

    final def now[A](effect: A): IO[A]     = ZIO.now(effect)
    final def point[A](effect: =>A): IO[A] = ZIO.point(effect)
    final def pure[A](effect: =>A): IO[A]  = ZIO.point(effect)

    final def sync[A](effect: =>A): IO[A] = ZIO.sync(effect)
    final def async[A](
      register: (scalaz.ioeffect.ExitResult[Throwable, A] => Unit) => Unit
    ): IO[A] = ZIO.async(register)

    final def fail[A](error: Throwable): IO[A] = ZIO.fail(error)

    final def unit: IO[Unit] = ZIO.unit

    import scala.concurrent.duration.Duration
    final def sleep(duration: Duration): IO[Unit] = ZIO.sleep(duration)

    import scala.concurrent.{ ExecutionContext, Future }
    final def fromFuture[E, A](io: IO[Future[A]]): IO[A] =
      ZIO.fromFuture(io)(ExecutionContext.global)
  }

  type MonadIO[F[_]] = scalaz.ioeffect.MonadIO[F, Throwable]
  object MonadIO {
    @inline def apply[F[_]](implicit F: MonadIO[F]): MonadIO[F] = F
  }

  /**
   * MonadIO plus MonadError specialised to the IO's errors
   *
   * Avoid using this if you can.
   */
  trait Sync[F[_]] extends MonadError[F, Throwable] {
    def liftIO[A](io: IO[A]): F[A]
  }
  object Sync {
    @inline def apply[F[_]](implicit F: Sync[F]): Sync[F] = F

    // intentionally not implicit, use this to opt-in to derive an instance
    def gen[F[_]](
      implicit F: MonadError[F, Throwable],
      I: MonadIO[F]
    ): Sync[F] =
      new Sync[F] {
        def point[A](a: =>A): F[A]                   = F.point(a)
        def bind[A, B](fa: F[A])(f: A => F[B]): F[B] = F.bind(fa)(f)

        def handleError[A](fa: F[A])(f: Throwable => F[A]): F[A] =
          F.handleError(fa)(f)
        def raiseError[A](e: Throwable): F[A] = F.raiseError(e)

        // assumes M is coherent with F
        def liftIO[A](io: IO[A]): F[A] = I.liftIO(io)(F)

        // perf
        override def ap[A, B](fa: =>F[A])(f: =>F[A => B]): F[B] =
          F.ap(fa)(f)
        override def map[A, B](fa: F[A])(f: A => B): F[B] = F.map(fa)(f)
        override def apply2[A, B, C](fa: =>F[A], fb: =>F[B])(
          f: (A, B) => C
        ): F[C] = F.apply2(fa, fb)(f)
      }

    implicit val ioSync: Sync[IO] = gen[IO]

    implicit def eitherTSync[F[_], E](
      implicit F: Sync[F]
    ): Sync[EitherT[F, E, ?]] =
      new Sync[EitherT[F, E, ?]] {
        def point[A](a: =>A): EitherT[F, E, A] = EitherT.pure(a)
        def bind[A, B](fa: EitherT[F, E, A])(
          f: A => EitherT[F, E, B]
        ): EitherT[F, E, B] = fa.flatMap(f)

        def handleError[A](fa: EitherT[F, E, A])(
          f: Throwable => EitherT[F, E, A]
        ): EitherT[F, E, A] = EitherT(fa.run.handleError(t => f(t).run))

        def raiseError[A](e: Throwable): EitherT[F, E, A] =
          EitherT.eitherT(F.raiseError(e))

        def liftIO[A](io: IO[A]): EitherT[F, E, A] =
          EitherT.rightT(F.liftIO(io))
      }

    implicit def stateTSync[F[_], S](
      implicit F: Sync[F]
    ): Sync[StateT[F, S, ?]] =
      new Sync[StateT[F, S, ?]] {
        def point[A](a: =>A): StateT[F, S, A] = StateT.stateT(a)
        def bind[A, B](fa: StateT[F, S, A])(
          f: A => StateT[F, S, B]
        ): StateT[F, S, B] = fa.flatMap(f)

        def handleError[A](fa: StateT[F, S, A])(
          f: Throwable => StateT[F, S, A]
        ): StateT[F, S, A] =
          StateT(s => F.handleError(fa.run(s))(e => f(e).run(s)))
        def raiseError[A](e: Throwable): StateT[F, S, A] =
          StateT.liftM(F.raiseError(e))

        def liftIO[A](io: IO[A]): StateT[F, S, A] = StateT.liftM(F.liftIO(io))
      }
  }

  implicit final class SyncCatsOps[F[_]](private val F: Sync[F]) {
    def suspend[A](fa: =>F[A]): F[A] = F.join(F.liftIO(IO(fa)))
    def delay[A](a: =>A): F[A]       = F.liftIO(IO(a))
  }

  // WORKAROUND https://github.com/scala/bug/issues/10954
  implicit val taskParAp: Applicative[IO.Par] = ZIO.taskParAp

  private object RTSCats extends scalaz.ioeffect.RTS
  implicit class TaskCatsOps[A](val io: IO[A]) {
    def unsafeRunSync(): A = RTSCats.unsafePerformIO(io.attempt[Void]) match {
      case -\/(underlying) => throw underlying
      case \/-(a)          => a
    }

    def unsafeRunSync[B](handler: Throwable \/ A => B): B =
      handler(RTSCats.unsafePerformIO(io.attempt))

    def unsafeToFuture(): scala.concurrent.Future[A] =
      RTSCats.unsafeTaskToFuture(io)
    def unsafeRunAsync(
      cb: Either[Throwable, A] => Unit
    ): Unit = {
      import scalaz.ioeffect.ExitResult
      @silent val k: ExitResult[Throwable, A] => Unit = {
        case ExitResult.Completed(a)  => cb(Right(a))
        case ExitResult.Failed(e)     => cb(Left(e))
        case ExitResult.Terminated(e) => cb(Left(e))
      }
      RTSCats.unsafePerformIOAsync(io)(k)
    }
  }

  implicit final class ExtraEitherCatsOps[A, B](private val v: Either[A, B]) {
    def toValidatedNel: ValidationNel[A, B] = v.disjunction.validationNel
  }

  implicit final class ExtraEitherCompanionCatsOps(
    private val self: Either.type
  ) {
    def fromOption[A, B](o: Option[B], ifNone: =>A): Either[A, B] = o match {
      case None    => Left(ifNone)
      case Some(a) => Right(a)
    }
  }

  implicit final class ExtraEitherTCatsOps[F[_], E, A](
    private val self: EitherT[F, E, A]
  ) {
    def value(implicit F: Functor[F]): F[Either[E, A]] =
      self.run.map(_.toEither)
  }

  implicit final class ExtraEitherTCompanionCatsOps(
    private val self: EitherT.type
  ) {
    def fromOption[F[_], A, B](
      o: Option[B],
      ifNone: =>A
    )(implicit F: Applicative[F]): EitherT[F, A, B] =
      EitherT.fromEither[F, A, B](F.pure(Either.fromOption(o, ifNone)))

    def liftF[F[_]: Functor, A, B](fb: F[B]): EitherT[F, A, B] = self.rightT(fb)
  }

  implicit final class ExtraValidationCatsOps[A, B](
    private val v: Validation[A, B]
  ) {
    def toValidatedNel: ValidationNel[A, B] = v.toValidationNel
  }

  implicit final class ValidationCatsOps[A, B](
    private val v: Validation[A, B]
  ) {
    def isInvalid: Boolean = v.isFailure
    def isValid: Boolean   = v.isSuccess
  }

  implicit final class NonEmptyListCompanionCatsOps(
    private val self: NonEmptyList.type
  ) {
    def fromList[A](lst: List[A]): Option[NonEmptyList[A]] = lst.toNel
    def of[A](h: A, t: A*): NonEmptyList[A]                = NonEmptyList.nels(h, t: _*)
    def one[A](x: A): NonEmptyList[A]                      = NonEmptyList(x)
    def fromFoldable[F[_], A](
      fa: F[A]
    )(implicit F: Foldable[F]): Option[NonEmptyList[A]] =
      fromList(F.toList(fa))

    def fromListUnsafe[A](x: List[A]): NonEmptyList[A] = x.unsafeToNel
  }

  implicit final class ExtraAnyCatsOps[A](private val self: A) {
    def valid[B]: Validation[B, A]         = self.success
    def validNel[B]: ValidationNel[B, A]   = self.successNel
    def invalid[B]: Validation[A, B]       = self.failure
    def invalidNel[B]: ValidationNel[A, B] = self.failureNel
    def eitherLeft[B]: Either[A, B]        = Left[A, B](self)
    def eitherRight[B]: Either[B, A]       = Right[B, A](self)
  }

  implicit final class OptionCatsOps[A](private val self: Option[A]) {
    def toValid[E](e: =>E): Validation[E, A] = self.toSuccess(e)
  }

  implicit final class StateTCatsOps[F[_], S, A](
    private val self: StateT[F, S, A]
  ) {
    def runA(initial: S)(implicit F: Monad[F]): F[A] = self.eval(initial)
    def runEmptyA[S1 <: S](implicit F: Monad[F], S: Monoid[S1]): F[A] =
      self.evalZero
  }

  implicit final class StateTCompanionCatsOps(private val self: StateT.type) {
    def liftF[F[_]: Monad, S, A](fa: F[A]): StateT[F, S, A] =
      self.liftM[F, S, A](fa)
  }

  implicit final class ExtraTheseCatsOps[L, R](private val self: L \&/ R) {
    // lossy!
    def disjunction: L \/ R = self.fold(_.left, _.right, (_, r) => r.right)
    def either: Either[L, R] =
      self.fold(_.eitherLeft, _.eitherRight, (_, r) => r.eitherRight)

    final def putLeft[C](left: C): C \&/ R =
      self.fold(
        _ => left.wrapThis,
        b => (left, b).both,
        (_, b) => (left, b).both
      )
    final def putRight[C](right: C): L \&/ C =
      self.fold(
        a => (a, right).both,
        _ => right.that,
        (a, _) => (a, right).both
      )
  }

  implicit final class ExtraBindCatsOps[F[_], A](
    val self: F[A]
  )(implicit val F: Bind[F]) {
    def flatten[B](implicit ev: A <~< F[B]): F[B] = self.join(ev)
  }

  implicit final class ExtraSemigroupCatsOps[F](
    val self: F
  )(implicit val F: Semigroup[F]) {
    def combine(other: =>F): F = self |+| other
  }

  implicit final class ExtraOptionCatsOps[A](val self: Option[A]) {
    def toValidNel[E](e: =>E): ValidatedNel[E, A]   = self.toSuccessNel(e)
    def toInvalidNel[B](b: =>B): ValidatedNel[A, B] = self.toFailureNel(b)
  }

  implicit val catsEffectInstance: cats.effect.Effect[IO] =
    scalaz.ioeffect.catz.catsEffectInstance
}
